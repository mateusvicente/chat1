Rails.application.routes.draw do

  resources :posts do
    resources :messages, only: [:create, :index]
  end

  root to: 'messages#index'

end