class AddInputsToMessage < ActiveRecord::Migration
  def up
    add_column :messages, :username, :string
    add_column :messages, :post_id, :string
  end

  def down
    remove_column :messages, :username
    remove_column :messages, :post_id
  end
end
