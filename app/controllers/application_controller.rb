class ApplicationController < ActionController::Base

  helper_method :username

  protect_from_forgery with: :exception

  before_filter :set_username

  protected

  def set_username
    if params[:username].present?
      session[:username] = params[:username]
      redirect_to request.path
    end
  end

  def username
    session[:username] || 'Anonimo'
  end

end