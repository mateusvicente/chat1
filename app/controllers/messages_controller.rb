 class MessagesController < ApplicationController

  def index
    @post = Post.find(params[:post_id])
    @messages = @post.messages.order(created_at: :asc).last(50)

    respond_to do |format|
      format.html
      format.json do
        render json: @messages
      end
    end
  end

  def create
    @post = Post.find(params[:post_id])
    @message = @post.messages.create!(message_params)
  end

  private

  def message_params
    params.require(:message).permit(:content).merge(username: username)
  end
end