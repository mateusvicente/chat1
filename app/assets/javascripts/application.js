//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

var WEBINARIO = WEBINARIO || {};

$(document).ready(function() {
  WEBINARIO.init();
});

$('body.webinars-show').ready(function() {
  WEBINARIO.videoVimeoControl();
});

$(window).resize(function() {
  WEBINARIO.contentCalcHeight();
});

WEBINARIO.closeFlash = function() {
  $(document).on('click', '*[data-close-flash]', function() {
    $(this).parent().hide();
    return false;
  });
};

WEBINARIO.init = function() {
  this.closeFlash();
  this.contentCalcHeight();
};

WEBINARIO.videoVimeoControl = function() {
  var iframe = $('#player1')[0];
  var player = $f(iframe);

  player.addEvent('ready', function() {
    player.addEvent('ready', onReady);
    player.addEvent('finish', onFinish);
    player.addEvent('playProgress', onPlayProgress);
    player.api('setVolume',1);
  });

  $('button').bind('click', function() {
    player.api($(this).text().toLowerCase());
  });

  var $initialTime = Cookies.set('currentTime');

  function setVideoTime() {
    var $target = $('*[data-video]:first');
    var $videoUrl = $target.attr('src') + $initialTime;
    $target.attr('src', $videoUrl);
  }

  // Verifica se existe... se existir, coloca tempo no vídeo, se não, começa do zero
  if($initialTime) {
    setVideoTime($initialTime);
  } else {
    $initialTime = '&#t=0m00s';
    setVideoTime($initialTime);
  }

  // Pega tempo atual, converte e seta como cookie
  function getTime(currentTime, $value) {
    var $currentTime = currentTime;
    var $time = ($currentTime/60.0).toString().split('.');
    var $hour = $time[0];
    var $min = $time[1]+'0';
    var $convertedTime = '&#t=' + $hour + 'm' + $min.slice(0,2) + 's';
    var $initialTime = $convertedTime;
    Cookies.set('currentTime', $initialTime);
  }

  // Calcula progresso...
  function onPlayProgress(data, id) {
    var $time = data.seconds;
    $convertedTime = getTime($time);
  }

  function onReady(id) {}

  function onFinish(id) {
    console.log('fim');
  }
};

WEBINARIO.contentCalcHeight = function() {
  var $content       = $('*[data-content]:first');
  var $header        = $('*[data-header]:first');
  var $pageHeight    = $(window).height();
  var $headerHeight  = $header.height();

  $content.css({
    'height': $pageHeight - $headerHeight
  })
};